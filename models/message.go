package models

import "log"

type Message struct {
	Message string `json:"message"`
}

var msgMemory []Message

func InitMsgMemory() {
	msgMemory = make([]Message, 0)

	log.Println("In-memory storage was initialised.")
}

func CreateMessage(message Message) Message {
	msgMemory = append(msgMemory, message)

	log.Println("Data saved in in-memory storage.")

	return msgMemory[len(msgMemory)-1]
}

func GetAllMessages() []Message {

	log.Println("Data receiving from in-memory storage.")

	return msgMemory
}

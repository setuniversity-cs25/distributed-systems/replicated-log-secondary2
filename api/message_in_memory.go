package api

import (
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"

	"gitlab.com/setuniversity-cs25/distributed-systems/replicated-log/models"
)

func messageInMemoryGetAll(c *gin.Context) {
	storedMessages := models.GetAllMessages()

	c.JSON(http.StatusOK, storedMessages)
}

func messageInMemoryCreate(c *gin.Context) {
	log.Println("secondary 2 storing message sterted")
	var message models.Message
	if err := c.ShouldBindJSON(&message); err != nil {
		log.Println("error occured on parsing in messageInMemoryCreate")
		c.Error(err)
		return
	}

	createdMessage := models.CreateMessage(message)
	log.Println("secondary 2 message stored")

	log.Println("secondary 2 sleep started")
	time.Sleep(5 * time.Second)
	log.Println("secondary 2 sleep finished")

	log.Println("secondary 2 storing message finished")
	c.JSON(http.StatusCreated, createdMessage)
}
